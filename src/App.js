import React, { Component } from 'react';

import Header from './components/Header/Header';
import LocationInput from './containers/LocationInput/LocationInput';
import Today from './containers/Today/Today';
import Weekly from './containers/Weekly/Weekly';

import './App.css';

class App extends Component {
  state = {
    today: 38,
    week: [
      {
        day: 'Thu',
        temperature: 28
      },
      {
        day: 'Thu',
        temperature: 42
      },
      {
        day: 'Thu',
        temperature: 34
      },
      {
        day: 'Thu',
        temperature: 22
      },
      {
        day: 'Thu',
        temperature: 26
      },
      {
        day: 'Thu',
        temperature: 31
      }
    ]
  }

  render() {
    return (
      <div className="container">
        <Header title="Daily Weather" description="What's weather today?" />
        <br />
        <LocationInput />
        <br />
        <br />
        <br />
        <div className="row align-items-center">
          <div className="col-md-5">
            <Today temperature={this.state.today} />
          </div>
          <div className="col-md">
            <Weekly week={this.state.week} />
          </div>
        </div>
      </div>
    );
  }
}

export default App;
