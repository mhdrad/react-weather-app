import React, { Component } from 'react'

import WeatherIcon from '../../../components/WeatherIcon/WeatherIcon'

export default class Day extends Component {
  render() {
    return (
      <div>
        <div>
            {this.props.day}
        </div>
        <div>
            <WeatherIcon temperature={this.props.temperature} />
        </div>
        <div>
            {this.props.temperature}
        </div>
      </div>
    )
  }
}
