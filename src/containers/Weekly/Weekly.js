import React, { Component } from 'react';
import Day from './Day/Day';

import classes from './Weekly.scss';

export default class Weekly extends Component {
    render() {
        return (
            <div className={classes.Weekly}>
                <div className="row">
                    {this.props.week.map(item => (
                        <div className="col">
                            <Day temperature={item.temperature} day={item.day} />
                        </div>
                    ))}
                </div>
            </div>
        )
    }
}
