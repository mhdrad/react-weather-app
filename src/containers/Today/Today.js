import React, { Component } from 'react'
import classes from './Today.scss';
import WeatherIcon from '../../components/WeatherIcon/WeatherIcon';

export default class Today extends Component {
    render() {
        return (
            <div className={classes.Today}>
                <div className="row">
                    <div className="col text-right">
                        <WeatherIcon temperature={this.props.temperature} />
                    </div>
                    <div className="col text-left">
                        <span className={classes.Temperature}>
                            {this.props.temperature}
                        </span>
                    </div>
                </div>
            </div>
        )
    }
}
