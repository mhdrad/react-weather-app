import React, { Component } from 'react'

import classes from './LocationInput.scss';

export default class LocationInput extends Component {
  render() {
    return (
      <div className={classes.LocationInput}>
        <a className={classes.Spinner} href-void>
          <i className="icon-mustache"></i>
        </a>
        <input
          type="text"
          class="form-control"
          aria-describedby="location"
          placeholder="Enter your location: Country, City"
        />
        <a className={classes.LocationFinder} href-void>
          <i className="icon-location-pin"></i>
        </a>
      </div>

    )
  }
}
