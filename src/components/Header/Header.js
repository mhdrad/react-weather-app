import React from 'react';

const Header = (props) => (
    <div>
        <h1 className="display-1">
            {props.title}
        </h1>
        <h2 class="text-muted h2">{props.description}</h2>
    </div>
)

export default Header;