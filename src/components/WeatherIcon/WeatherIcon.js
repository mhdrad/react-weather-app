import React from 'react';

const getIcon = (temperature) => {
    if(temperature > 35)
        return 'wi-hot';
    if(temperature > 30)
        return 'wi-day-sunny';
    if(temperature > 25)
        return 'wi-day-cloudy';
    if(temperature > 20)
        return 'wi-day-sunny-overcast';
}

const WeatherIcon = (props) => (
    <i className={`wi ${getIcon(props.temperature)}`}></i>
)

export default WeatherIcon;